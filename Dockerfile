FROM golang:1.12.7-alpine3.10 AS build
# Support CGO and SSL
RUN apk --no-cache add gcc g++ make
RUN apk add git
WORKDIR /go/src/app
COPY . .
COPY .env ./config/
RUN ls .env
RUN ls ./config/.env
RUN go get golang.org/x/crypto/bcrypt
RUN go get github.com/gorilla/mux
RUN go get github.com/jinzhu/gorm
RUN go get github.com/dgrijalva/jwt-go
RUN go get github.com/joho/godotenv
RUN go get github.com/lib/pq
RUN go get github.com/lib/pq/hstore
RUN GOOS=linux go build -ldflags="-s -w" -o ./bin/absensi ./main.go

FROM alpine:3.10
RUN apk --no-cache add ca-certificates
WORKDIR /usr/bin
COPY --from=build /go/src/app/bin /go/bin
COPY --from=build /go/src/app/.env  /go/bin/
EXPOSE 8080
ENTRYPOINT /go/bin/absensi --port 8080

