package config

import (
	
	"fmt"
	//"log"
	//"os"
    "app/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //Gorm postgres dialect interface
	//"github.com/joho/godotenv"
)

//ConnectDB function: Make database connection
func ConnectDB() *gorm.DB {

	//Load environmenatal variables
/*	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file db config",err)
	}

	username := os.Getenv("databaseUser")
	password := os.Getenv("databasePassword")
	databaseName := os.Getenv("databaseName")
	databaseHost := os.Getenv("databaseHost")
*/

	//Define DB connection string
	//dbURI := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", databaseHost, username, databaseName, password)

	//connect to db URI
	db, err := gorm.Open("postgres", "host=172.25.0.2 port=5432 user=postgres dbname=oc_absensi password=postgres sslmode=disable")

	if err != nil {
		fmt.Println("error", err)
		panic(err)
	}
	// close db when not in use
	//defer db.Close()

	// Migrate the schema
	db.AutoMigrate(
		&models.User{})

	fmt.Println("Successfully connected!", db)
	return db
}