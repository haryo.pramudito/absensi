package main

import (
	//"fmt"
	"log"
	"net/http"
	"app/routes"
//	"github.com/joho/godotenv"
	//"os"
)

func main() {

/* 	e := godotenv.Load()
	if e != nil {
		log.Fatal("Error loading .env file Main")
	}
	fmt.Println(e) */

	

	// Handle routes
	http.Handle("/", routes.Handlers())

	// serve
	log.Printf("Server up on port '%s'", 8080)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

